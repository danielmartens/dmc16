import numpy as np
import features.all_features

### get target data
def getY(data):
    possible_y = np.array([0, 1, 2, 5, 3, 4])

    new_y = np.empty([data.shape[0], 3])

    #for u in np.arange(possible_y.shape[0]):
    new_y[:, 0] = data['returnQuantity'].isin([0])
    new_y[:, 1] = data['returnQuantity'].isin([1])
    new_y[:, 2] = data['returnQuantity'].isin(np.array([2, 3, 4, 5]))

    #return new_y
    return np.asarray(data['returnQuantity'])


### get feature set
def getX(data , test_set=False, verbose=False):

#    feature_methodes = [ "productGroup_feature", "feature_price", "feature_rrp", "feature_price_rrp_ratio",\
#     "feature_voucher_used", "feature_voucher_amount", "feature_quantity", "feature_deviceID",\
#     "feature_paymentMethod", "feature_frequent_buyer", "feature_basket_length", "feature_week", \
#     "feature_month", "feature_holiday", "feature_xmas", "feature_num_purchase", "feature_days_since_last",\
#                      "feature_weekday" ]

#    feature_methodes = [ "feature_price", "feature_rrp", "feature_price_rrp_ratio",\
#     "feature_voucher_used", "feature_voucher_amount", "feature_quantity", "feature_deviceID",\
#     "feature_paymentMethod", "feature_frequent_buyer", "feature_basket_length"]

    feature_methodes = ["feature_price", "feature_rrp", "feature_voucher_used", "feature_voucher_amount", \
                        "feature_quantity", "feature_deviceID", \
                        "feature_paymentMethod", "feature_frequent_buyer", "feature_basket_length"]


    if test_set == False:
        feature_methodes = feature_methodes + ["train_features_ReturnRatio"]
    else:
        feature_methodes = feature_methodes + ["test_features_ReturnRatio"]

    feats = []
    names = []

    for m in feature_methodes:
        func = getattr(features.all_features, m)
        f, n = func(data)
        if f.ndim == 1:
            f = f.reshape([f.shape[0],1])
        if verbose:
            print(m)
            print(f.shape)
        feats.append(f)
        names.append(n)

    data_set = np.concatenate([f for f in feats], axis=1).astype(float)

    if verbose:
        print(data_set.shape)
        print(sum(names, []))
        print([item for sublist in names for item in sublist])

    return data_set, sum(names, [])