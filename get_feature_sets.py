import pandas as pd
import numpy as np
import klepto
#import pickle

from feature_gen import getY, getX
from features.training_based_features import train_features_ReturnRatio, test_features_ReturnRatio

## generate the feature set and dump it with klepto

### load data sets
print("load all data")
data_test = pd.read_csv('test_20.csv',sep = ';')
data_train = pd.read_csv('train_80.csv',sep = ';')

#sort data for orderID
data_test = data_test.sort_values('orderID')
data_train = data_train.sort_values('orderID')


print("get train features")
train_x, names = getX(data_train)
train_y = getY(data_train)

print("get test features")
test_x, names_test = getX(data_test, test_set=True)
test_y = getY(data_test)

d = klepto.archives.file_archive("feature_sets.klepto", cached=True, serialized=True)
d['trainX'] = train_x
d['trainY'] = train_y
d['testX'] = test_x
d['testY'] = test_y
d['names'] = names
d.dump()
d.clear()


#pickle.dump(train_x, open( "train_x_features.p", "wb" ))
#pickle.dump(train_y, open( "train_y_features.p", "wb" ))
#pickle.dump(test_x, open( "test_x_features.p", "wb" ))
#pickle.dump(test_y, open( "test_y_features.p", "wb" ))

