# Choose Classifier Here and Give it a Name
from sklearn.ensemble import RandomForestClassifier
name = "Random Forest "


#The rest stayes the same with default settings, change as you like
from sklearn.metrics import accuracy_score, precision_recall_fscore_support
from sklearn.metrics import confusion_matrix
from util import dmc16_eval
import matplotlib.pyplot as plt
import numpy as np



def feature_impportance_rf(train_x, train_y, test_x, test_y, names):
    print("Start",name,"Classifier:")
    model =RandomForestClassifier(n_estimators=2)

    print("Train",name,"Classifier:")
    train = model.fit(train_x, train_y)

    print("Test",name,"Classifier:")
    pred = model.predict(test_x)

    # featuer importance
    importance = model.feature_importances_
    std = np.std([tree.feature_importances_ for tree in model.estimators_], axis=0)
    indices = np.argsort(importance)[::-1]


    for i in range(indices.shape[0]):
        print(str(i) + " " + names[i] + " " + str(importance[i]))

    #bar chart plott for feature importance
    #because of the huge number of the features, legend of the bars are shown in the terminal
    plt.figure(figsize=(30, 10))
    plt.title("feature importance")
    plt.bar(range(importance.shape[0]), importance, color="r", align="center")
    plt.xlim([-1, importance.shape[0]])
    plt.xticks(range(importance.shape[0]))
    plt.show()

    dmc_eval = dmc16_eval(pred, test_y)
    return dmc_eval





