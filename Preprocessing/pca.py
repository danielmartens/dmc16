import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import klepto

from sklearn.decomposition import PCA
from feature_gen import getY, getX
from util import dmc16_eval


### load data sets

d = klepto.archives.file_archive("feature_sets.klepto", cached=True,serialized=True)
d.load()
train_x = d['train_x']
train_y = d['train_y']
test_x  = d['test_x']
test_y  = d['test_y']
d.clear()
print("got features")

## build classifier
print(train_x.shape)
print(test_x.shape)

pca = PCA(n_components=0.9) #train_x.shape[1])
pca.fit(train_x)
#pcadat = pca.fit_transform(train_x)
pc = pca.explained_variance_

print(pc)
plt.bar(np.arange(pc.shape[0]), pca.explained_variance_ratio_)
plt.show()

new_train_x = pca.transform(train_x)
new_test_x = pca.transform(test_x) #todo correct test train
print(new_train_x.shape)

c = klepto.archives.file_archive("PCA.klepto", cached=True, serialized=True)
c['train_x'] = new_train_x
c['train_y'] = train_y
c['test_x'] = new_test_x
c['test_y'] = test_y
c.dump()
c.clear()