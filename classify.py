import klepto
import numpy as np
import pandas as pd
from features.pca import pca
from util import dmc16_eval
from Classifiers.SGD import classifier_sgd
from Classifiers.RandomForest import classifier_rf
from Classifiers.LogisticRegression import classifier_lr
from Classifiers.NaiveBayes import classifier_nb
from Preprocessing.RandomForest import feature_impportance_rf
from Classifiers.NeuralNetwork import classifier_nn
### load data sets

import features.all_features as all_features

def getY(data):
    return np.asarray(data['returnQuantity'])

def getX(data , test_set=False):

    feature_methodes = [  "feature_price", "feature_voucher_used",\
                          "feature_voucher_amount", "feature_quantity", "feature_deviceID",\
                          "feature_paymentMethod", "feature_frequent_buyer", "feature_basket_length", "feature_rrp" ]

    if test_set == False:
        feature_methodes = feature_methodes + ["train_features_ReturnRatio"]
    else:
        feature_methodes = feature_methodes + ["test_features_ReturnRatio"]

    feats = []
    names = []

    for m in feature_methodes:
        func = getattr(all_features, m)
        f, n = func(data)
        if f.ndim == 1:
            f = f.reshape([f.shape[0],1])
        feats.append(f)
        names.append(n)

    data_set = np.concatenate([f for f in feats], axis=1).astype(float)
    return data_set, sum(names, [])


data_train = pd.read_csv('train_80.csv', sep = ';')
data_test = pd.read_csv('test_20.csv', sep = ';')

print("get train features")
train_x, names = getX(data_train)
train_y = getY(data_train)

print("get test features")
test_x, names_test = getX(data_test, test_set=True)
test_y = getY(data_test)

#uncomment for pca
#train_x, test_x = pca(train_x, test_x, components=41)

## build classifier
print(train_x.shape)
print(test_x.shape)

# Classifier Methods from Folder Classifiers
#feature_selection = feature_impportance_rf (train_x, train_y, test_x, test_y, names)
dmc_eval = classifier_nn(train_x, train_y, test_x, test_y)

