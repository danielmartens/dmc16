# Choose Classifier Here and Give it a Name
from sklearn.ensemble import RandomForestClassifier
name = "Random Forest "


#The rest stayes the same with default settings, change as you like
from sklearn.metrics import accuracy_score, precision_recall_fscore_support
from sklearn.metrics import confusion_matrix
from util import dmc16_eval

def classifier_rf(train_x, train_y, test_x, test_y):
    print("Start",name,"Classifier:")
    model =RandomForestClassifier(n_estimators=10)

    print("Train",name,"Classifier")
    train = model.fit(train_x, train_y)

    print("Test",name,"Classifier")
    pred = model.predict(test_x)

    print("Evaluate",name,"Classifier")
    print("Confusion Matrix")
    print(confusion_matrix(test_y, pred))
    print("Accuracy")
    print(accuracy_score(test_y, pred))
    print("DMC Eval")
    dmc_eval = dmc16_eval(pred, test_y)
    print(dmc_eval)
    return dmc_eval





