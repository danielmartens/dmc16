from sklearn.linear_model import SGDClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from util import dmc16_eval

name = "Stochastic Gradient Descent"

def classifier_sgd(train_x, train_y, test_x, test_y):
    print("Start",name,"Classifier:")
    model = SGDClassifier()

    print("Train",name,"Classifier")
    train = model.fit(train_x, train_y)

    print("Test",name,"Classifier")
    pred = model.predict(test_x)

    print("Evaluate",name,"Classifier")
    print("Confusion Matrix")
    print(confusion_matrix(test_y, pred))
    print("Accuracy")
    print(accuracy_score(test_y, pred))
    print("DMC Eval")
    dmc_eval = dmc16_eval(pred, test_y)
    print(dmc_eval)
    return dmc_eval