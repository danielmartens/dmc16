# Choose Classifier Here and Give it a Name
from sklearn.linear_model import LogisticRegression
name = "Logistic Regression"


#The rest stayes the same with default settings, change as you like
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from util import dmc16_eval



def classifier_lr(train_x, train_y, test_x, test_y):
    print("Start",name,"Classifier:")
    model = LogisticRegression()

    print("Train",name,"Classifier")
    train = model.fit(train_x, train_y)

    print("Test",name,"Classifier")
    pred = model.predict(test_x)

    print("Evaluate",name,"Classifier")
    print("Confusion Matrix")
    print(confusion_matrix(test_y, pred))
    print("Accuracy")
    print(accuracy_score(test_y, pred))
    print("DMC Eval")
    dmc_eval = dmc16_eval(pred, test_y)
    print(dmc_eval)
    return dmc_eval