import pandas as pd
import numpy as np
import holidays


def feature_weekday(data):
    data['orderDate'] = pd.to_datetime(data['orderDate'], format='%Y-%m-%d')
    df = pd.DataFrame()

    for i in range(0, 6):
        df['weekday_' + str(i)] = data['orderDate'].dt.dayofweek == i

    return df, list(df.column.values)


def feature_week(data):
    data['orderDate'] = pd.to_datetime(data['orderDate'], format='%Y-%m-%d')
    d = np.asarray((data['orderDate'].dt.week - np.mean(data['orderDate'].dt.week)) / np.std(data['orderDate'].dt.week))
    return d, ["week"]


def feature_month(data):
    data['orderDate'] = pd.to_datetime(data['orderDate'], format='%Y-%m-%d')
    d = np.asarray((data['orderDate'].dt.month - np.mean(data['orderDate'].dt.month)) / np.std(data['orderDate'].dt.month))
    return d, ["month"]


def feature_holiday(data):
    data['orderDate'] = pd.to_datetime(data['orderDate'], format='%Y-%m-%d')
    de_holidays = holidays.DE()
    d = np.asarray(data['orderDate'].dt.date.isin(de_holidays))
    return d, ["holiday"]


def feature_xmas(data):
    data['orderDate'] = pd.to_datetime(data['orderDate'], format='%Y-%m-%d')
    d = np.asarray(data['orderDate'].dt.week > 46)
    return d, ["xmas"]


def feature_num_purchase(data):
    data['id'] = data.index
    temp_colums = np.zeros([data[[0]].__len__(), 2])
    sortedData = data.sort_values(['customerID', 'orderID'], ascending=[True, True])
    temp_colums[:, 0] = sortedData['id']
    for index, row in sortedData.iterrows():
        if var is not row['customerID']:
            var = row['customerID']
            date_old = row['orderDate']
            number = -1
        number += 1
        temp_colums[index, 1] = number

        date_old = row['orderDate']

    temp_colums[temp_colums[:, 0].argsort()]
    data['numPurchase'] = temp_colums[:, 1]
    d = np.asarray((data["numPurchase"] - np.mean(data["numPurchase"])) / np.std(data["numPurchase"]))

    return d, ["num_purchase"]


def feature_days_since_last(data):
    data['orderDate'] = pd.to_datetime(data['orderDate'], format='%Y-%m-%d')
    data['id'] = data.index
    temp_colums = np.zeros([data[[0]].__len__(), 2])
    sortedData = data.sort_values(['customerID', 'orderID'], ascending=[True, True])
    temp_colums[:, 0] = sortedData['id']
    for index, row in sortedData.iterrows():
        if var is not row['customerID']:
            var = row['customerID']
            date_old = row['orderDate']
        date_diff = row['orderDate'] - date_old
        temp_colums[index, 1] = date_diff
        date_old = row['orderDate']

    temp_colums[temp_colums[:, 0].argsort()]
    data['days_since_last'] = temp_colums[:, 1]
    d = np.asarray((data["days_since_last"] - np.mean(data["days_since_last"])) / np.std(data["days_since_last"]))

    return d, ["days_since_last"]



# def features_erik(data):
#     #data = pd.read_csv('orders_train.txt', sep=';')
#
#     #Date features generation
#
#     names = list(data.columns.values)
#
#
#     #Interpret Date column as datetime values
#     data['orderDate']= pd.to_datetime(data['orderDate'], format='%Y-%m-%d')
#
#     #Generate Day of Week values (0: Monday, ..., 6: Sunday)
#     data['weekday'] = data['orderDate'].dt.dayofweek
#
#     #Generate month values (0: January, ... ,11: December)
#     data['month'] = data['orderDate'].dt.month / 12
#
#     #Generate isoweek values (0: first week, ... ,51: last week)
#     data['week'] = data['orderDate'].dt.week / 52
#
#     #initialize weekday features
#     data['weekday_0'] = 0
#     data['weekday_1'] = 0
#     data['weekday_2'] = 0
#     data['weekday_3'] = 0
#     data['weekday_4'] = 0
#     data['weekday_5'] = 0
#     data['weekday_6'] = 0
#
#     #generate weekday features
#     data.loc[data['weekday'] == 0, 'weekday_0'] = 1
#     data.loc[data['weekday'] == 1, 'weekday_1'] = 1
#     data.loc[data['weekday'] == 2, 'weekday_2'] = 1
#     data.loc[data['weekday'] == 3, 'weekday_3'] = 1
#     data.loc[data['weekday'] == 4, 'weekday_4'] = 1
#     data.loc[data['weekday'] == 5, 'weekday_5'] = 1
#     data.loc[data['weekday'] == 6, 'weekday_6'] = 1
#
#     #Initialize end-of-year shopping feature
#     data['shopping_xmas'] = 0
#     #generate end-of-year shopping feature ( christmas shopping = shopping after calender week 48)
#     data.loc[data['week'] > 0.92, 'shopping_xmas'] = 1
#
#     #Initialize holiday list for germany
#     de_holidays = holidays.DE()
#     #Generate holiday feature
#     data['holidays'] = data['orderDate'].dt.date.isin(de_holidays)
#
#     #Enumerate Data for later use
#     data['id'] = data.index
#
#     #Generate NumberOfPurchase Feature
#     data['numPurchase'] = 0
#
#     #Generate temporal columns for later use
#     temp_colums = np.zeros([data[[0]].__len__(), 4])
#
#
#     sortedData = data.sort_values(['customerID', 'orderID'], ascending=[True, True])
#
#     temp_colums[:, 0] = sortedData['id']
#     #print(temp_colums[temp_colums[:,0].argsort()])
#
#     sortedData['numPurchase'] = 0
#     sortedData['DaysSinceLasePurchase'] = -1;
#
#     var = 'a'
#
#
#
#     for index, row in sortedData.iterrows():
#         if var is not row['customerID']:
#             var = row['customerID']
#             date_old = row['orderDate']
#             number = -1
#         number += 1
#         date_diff = row['orderDate'] - date_old
#         #print(date_diff.days)
#
#         #Eintrag in Spalte funktioniert nicht!!! BUG!
#         temp_colums[index, 1] = number
#         temp_colums[index, 2] = date_diff.days
#
#         date_old = row['orderDate']
#
#     temp_colums[temp_colums[:, 0].argsort()]
#     data['numPurchase'] = temp_colums[:, 1]
#     data['DaysSinceLastPurchase'] =temp_colums[:, 2]
#
#     sortedData.sort_values(['customerID', 'orderID'], ascending=[True, False])
#     temp_colums[:, 0] = sortedData['id']
#
#     var = 'a'
#     for i in sortedData.iterrows():
#         if var is not row['customerID']:
#             var = row['customerID']
#             max_purchase = row['numPurchase']
#         temp_colums[index, 3] = max_purchase
#
#     temp_colums[temp_colums[:, 0].argsort()]
#     data['maxPurchase'] = temp_colums[:, 3]
#
#     list_new = list(data.columns.values)
#
#     for i in range(len(names)):
#         list_new.pop(0)
#
#     erik_features = np.empty([data.shape[0], 14])
#     #pump features into np array to be used later
#     erik_features[:,0] = np.asarray(data['weekday_0'])
#     erik_features[:,1] = np.asarray(data['weekday_1'])
#     erik_features[:,2] = np.asarray(data['weekday_2'])
#     erik_features[:,3] = np.asarray(data['weekday_3'])
#     erik_features[:,4] = np.asarray(data['weekday_4'])
#     erik_features[:,5] = np.asarray(data['weekday_5'])
#     erik_features[:,6] = np.asarray(data['weekday_6'])
#     erik_features[:,7] = (pd.to_numeric(data['month']) - np.mean(data['month'])) / np.std(data['month'])
#     erik_features[:,8] = (pd.to_numeric(data['week']) - np.mean(data['week'])) / np.std(data['week'])
#     erik_features[:,9] = np.asarray(data['holidays'])
#     erik_features[:,10] = np.asarray(data['shopping_xmas'])
#     erik_features[:,11] = np.asarray(data['numPurchase'])
#     erik_features[:,12] = np.asarray(data['DaysSinceLastPurchase'])
#     erik_features[:,13] = np.asarray(data['maxPurchase'])
#
#
#     #return date feature set
#     return erik_features, list_new