import pandas as pd
import numpy as np

from pandas import DataFrame
from sklearn.preprocessing import StandardScaler


def feature_quantity(data):
    df = DataFrame()
    names = []

    for i in range(0,5):
        df[i] = np.asarray(data['quantity'] == i).astype(int)
        names.append("quantity_" + str(i))

    df[5] = np.asarray(data['quantity'] >= 5).astype(int)
    names.append("quantity_more_than_four")

    return np.asarray(df), names


def feature_deviceID(data):
    df = DataFrame()
    names = []

    for i in range(1,6):
        df[i] = np.asarray(data['deviceID'] == i).astype(int)
        names.append("deviceID_" + str(i))

    return np.asarray(df), names


def feature_paymentMethod(data):
    df = DataFrame()
    paymentMethods = data.paymentMethod.unique()
    names = []

    for i, method in enumerate(paymentMethods):
        df[i] = np.asarray(data['paymentMethod'] == method).astype(int)
        names.append('paymentMethod_' + str(i))

    return np.asarray(df), names


def feature_frequent_buyer(data):
    df = DataFrame()
    customerOrders = data.groupby('customerID').size()
    customerLabels = pd.qcut(customerOrders, [0, 0.5, 0.75, 1], labels=[0,1,2])
    df = data.join(DataFrame(customerLabels), on='customerID') #takes 2-3 seconds, faster?

    d = np.asarray(pd.get_dummies(df[0])) #3 binary features for the 3 categories of buyers
    names = ["infrequent_buyer", "frequent_buyer", "excessive_buyer"]
    return d, names



def feature_basket_length(data):
    orders = data.groupby('orderID').size()
    df = data.join(DataFrame(orders), on='orderID')
    baskets = df[0]
    baskets = (baskets-baskets.mean())/baskets.std()

    d = np.ndarray(shape=(data.shape[0], 1))
    d[:,0]= baskets

    return d, ["basketLength"]


