import pickle
import numpy as np

from pandas import DataFrame


def train_features_ReturnRatio(data):
	customer_rr = prepare_return_ratio_train(data, 'customerID')
	article_rr = prepare_return_ratio_train(data, 'articleID')
	features = np.concatenate([customer_rr, article_rr], axis=1).astype(float)
	return features, ['customerReturnRatio','articleReturnRatio']

def test_features_ReturnRatio(data):
	customer_rr = prepare_return_ratio_test(data, 'customerID')
	article_rr = prepare_return_ratio_test(data, 'articleID')
	features = np.concatenate([customer_rr, article_rr], axis=1).astype(float)
	return features, ['customerReturnRatio','articleReturnRatio']

def prepare_return_ratio_train(data, column_name):
	cr = data.groupby([column_name, 'returnQuantity']).size()
	cr = DataFrame(cr).reset_index('returnQuantity')
	cr['totalReturns'] = cr['returnQuantity']*cr[0]
	cr = DataFrame(cr['totalReturns']).reset_index().groupby(column_name).sum()

	ci = data.groupby([column_name, 'quantity']).size()
	ci = DataFrame(ci).reset_index('quantity')
	ci['totalItems'] = ci['quantity']*ci[0]	
	ci = DataFrame(ci['totalItems']).reset_index().groupby(column_name).sum()
	
	return_ratio = cr['totalReturns']/ci['totalItems'] #categorical evt?
	return_ratio = return_ratio.replace(np.inf, 1) #happens when totalItems = 0 but totalReturns > 0
	return_ratio = return_ratio.fillna(0) #happens when totalItems = 0, totalReturns = 0, usually the pathological case price=quantity=retQuantity=0

	return_ratio = (return_ratio-return_ratio.mean())/return_ratio.std()

	df = data.join(DataFrame(return_ratio), on=column_name)

	#write data via pickle

	pickle.dump(return_ratio, open(column_name+"_return_ratio.p", "wb" ))
	#load pickle
	#return_ratio = pickle.load(open( "return_ratio.p", "rb" ))

	#return feature array
	arr = np.ndarray(shape=(data.shape[0], 1))
	arr[:,0]= df[0]

	return arr

def prepare_return_ratio_test(data, column_name):
	
	return_ratio = pickle.load(open(column_name+"_return_ratio.p", "rb"))
	#rr_mean = return_ratio.mean()
	rr_mean = 0

	df = data.join(DataFrame(return_ratio), on=column_name)
	return_ratio = df[0].fillna(rr_mean)

	arr = np.ndarray(shape=(data.shape[0], 1))
	arr[:,0]= return_ratio
	
	return arr



