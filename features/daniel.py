import pandas as pd
import numpy as np


def feature_price(data):
    d = np.asarray(pd.to_numeric(data['price']) - np.mean(data['price']) )/ np.std(data['price'])
    return d, ["price"]


def feature_rrp(data):
    data['rrp'] = data['rrp'].fillna(data['price'])
    d = np.asarray((pd.to_numeric(data['rrp']) - np.mean(data['rrp'])) / np.std(data['rrp']))
    return d, ["rrp"]


def feature_price_rrp_ratio(data):
    data['price_rrp_ratio'] = np.max(data['price'] - data['rrp'], 0)
    d = pd.to_numeric(((data['price_rrp_ratio']) - np.mean(data['price_rrp_ratio'])) / np.std(data['price_rrp_ratio']))
    return d, ["price_rrp_ratio"]


def feature_voucher_used(data):
    d = np.asarray(data['voucherAmount'] > 0)
    return d, ["voucherUsed"]


def feature_voucher_amount(data):
    d = np.asarray(pd.to_numeric(data['voucherAmount']) - np.mean(data['voucherAmount'])) / np.std(data['voucherAmount'])
    return d, ["voucherAmount"]

data = pd.read_csv('test_20.csv',sep = ';')


