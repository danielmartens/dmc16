

import pandas as pd
from pandas import DataFrame
import numpy as np
import holidays
import pickle

def feature_product_group(data):

    train_data = pd.read_csv('orders_train.txt', sep=';')
    test_data = pd.read_csv('orders_class.txt', sep=';')

    all_data = pd.concat([ train_data, test_data])
    uniPG = all_data['productGroup'].unique()

    productGroupNP = np.zeros([data.shape[0],uniPG.shape[0]])
    names = []
    for u in np.arange(uniPG.shape[0]):
        productGroupNP[:,u] = data['productGroup'].isin([uniPG[u]])
        names.append("productGroup_" + str(uniPG[u]) )

    return productGroupNP, names

def feature_price(data):
    d = np.asarray(pd.to_numeric(data['price']) - np.mean(data['price']) )/ np.std(data['price'])
    return d, ["price"]


def feature_rrp(data):
    data['rrp'] = data['rrp'].fillna(data['price'])
    d = np.asarray((pd.to_numeric(data['rrp']) - np.mean(data['rrp'])) / np.std(data['rrp']))
    return d, ["rrp"]


def feature_price_rrp_ratio(data):
    data['price_rrp_ratio'] = np.max(data['price'] - data['rrp'], 0)
    d = pd.to_numeric(((data['price_rrp_ratio']) - np.mean(data['price_rrp_ratio'])) / np.std(data['price_rrp_ratio']))
    return d, ["price_rrp_ratio"]


def feature_voucher_used(data):
    d = np.asarray(data['voucherAmount'] > 0)
    return d, ["voucherUsed"]


def feature_voucher_amount(data):
    d = np.asarray(pd.to_numeric(data['voucherAmount']) - np.mean(data['voucherAmount'])) / np.std(data['voucherAmount'])
    return d, ["voucherAmount"]

data = pd.read_csv('test_20.csv',sep = ';')




def feature_quantity(data):
    df = DataFrame()
    names = []

    for i in range(0,5):
        df[i] = np.asarray(data['quantity'] == i).astype(int)
        names.append("quantity_" + str(i))

    df[5] = np.asarray(data['quantity'] >= 5).astype(int)
    names.append("quantity_more_than_four")

    return np.asarray(df), names


def feature_deviceID(data):
    df = DataFrame()
    names = []

    for i in range(1,6):
        df[i] = np.asarray(data['deviceID'] == i).astype(int)
        names.append("deviceID_" + str(i))

    return np.asarray(df), names


def feature_paymentMethod(data):
    df = DataFrame()

    train_data = pd.read_csv('orders_train.txt', sep=';')
    test_data = pd.read_csv('orders_class.txt', sep=';')

    all_data = pd.concat([ train_data, test_data])
    paymentMethods = all_data['paymentMethod'].unique()
    #paymentMethods = data.paymentMethod.unique()
    names = []

    for i, method in enumerate(paymentMethods):
        df[i] = np.asarray(data['paymentMethod'] == method).astype(int)
        names.append('paymentMethod_' + str(i))

    return np.asarray(df), names


def feature_frequent_buyer(data):
    df = DataFrame()
    customerOrders = data.groupby('customerID').size()
    customerLabels = pd.qcut(customerOrders, [0, 0.5, 0.75, 1], labels=[0,1,2])
    df = data.join(DataFrame(customerLabels), on='customerID') #takes 2-3 seconds, faster?

    d = np.asarray(pd.get_dummies(df[0])) #3 binary features for the 3 categories of buyers
    names = ["infrequent_buyer", "frequent_buyer", "excessive_buyer"]
    return d, names



def feature_basket_length(data):
    orders = data.groupby('orderID').size()
    df = data.join(DataFrame(orders), on='orderID')
    baskets = df[0]
    baskets = (baskets-baskets.mean())/baskets.std()

    d = np.ndarray(shape=(data.shape[0], 1))
    d[:,0]= baskets

    return d, ["basketLength"]


def feature_week(data):
    data['orderDate'] = pd.to_datetime(data['orderDate'], format='%Y-%m-%d')
    d = np.asarray((data['orderDate'].dt.week - np.mean(data['orderDate'].dt.week)) / np.std(data['orderDate'].dt.week))
    return d, ["week"]

def feature_month(data):
    data['orderDate'] = pd.to_datetime(data['orderDate'], format='%Y-%m-%d')
    d = np.asarray((data['orderDate'].dt.month - np.mean(data['orderDate'].dt.month)) / np.std(data['orderDate'].dt.month))
    return d, ["month"]

def feature_holiday(data):
    data['orderDate'] = pd.to_datetime(data['orderDate'], format='%Y-%m-%d')
    de_holidays = holidays.DE()
    d = np.asarray(data['orderDate'].dt.date.isin(de_holidays))
    return d, ["holiday"]


def feature_xmas(data):
    data['orderDate'] = pd.to_datetime(data['orderDate'], format='%Y-%m-%d')
    d = np.asarray(data['orderDate'].dt.week > 46)
    return d, ["xmas"]

def feature_num_purchase(data):
    data['id'] = data.index
    temp_colums = np.zeros([data[[0]].__len__(), 2])
    sortedData = data.sort_values(['customerID', 'orderID'], ascending=[True, True])
    temp_colums[:, 0] = sortedData['id']
    var = 'a'
    for index, row in sortedData.iterrows():
        if var is not row['customerID']:
            var = row['customerID']
            date_old = row['orderDate']
            number = -1
        number += 1
        temp_colums[index, 1] = number

        date_old = row['orderDate']

    temp_colums[temp_colums[:, 0].argsort()]
    data['numPurchase'] = temp_colums[:, 1]
    d = np.asarray((data["numPurchase"] - np.mean(data["numPurchase"])) / np.std(data["numPurchase"]))

    return d, ["num_purchase"]

def feature_days_since_last(data):
    data['orderDate'] = pd.to_datetime(data['orderDate'], format='%Y-%m-%d')
    data['id'] = data.index
    temp_colums = np.zeros([data[[0]].__len__(), 2])
    sortedData = data.sort_values(['customerID', 'orderID'], ascending=[True, True])
    temp_colums[:, 0] = sortedData['id']
    var = "a"
    for index, row in sortedData.iterrows():
        if var is not row['customerID']:
            var = row['customerID']
            date_old = row['orderDate']
        date_diff = row['orderDate'] - date_old
        temp_colums[index, 1] = date_diff.days
        date_old = row['orderDate']

    temp_colums[temp_colums[:, 0].argsort()]
    data['days_since_last'] = temp_colums[:, 1]
    d = np.asarray((data["days_since_last"] - np.mean(data["days_since_last"])) / np.std(data["days_since_last"]))

    return d, ["days_since_last"]

def feature_weekday(data):
    data['orderDate'] = pd.to_datetime(data['orderDate'], format='%Y-%m-%d')
    df = pd.DataFrame()

    for i in range(0, 6):
        df['weekday_' + str(i)] = data['orderDate'].dt.dayofweek == i

    return df, list(df.columns.values)


def train_features_ReturnRatio(data):
	customer_rr = prepare_return_ratio_train(data, 'customerID')
	article_rr = prepare_return_ratio_train(data, 'articleID')
	features = np.concatenate([customer_rr, article_rr], axis=1).astype(float)
	return features, ['customerReturnRatio','articleReturnRatio']

def test_features_ReturnRatio(data):
	customer_rr = prepare_return_ratio_test(data, 'customerID')
	article_rr = prepare_return_ratio_test(data, 'articleID')
	features = np.concatenate([customer_rr, article_rr], axis=1).astype(float)
	return features, ['customerReturnRatio','articleReturnRatio']

def prepare_return_ratio_train(data, column_name):
	cr = data.groupby([column_name, 'returnQuantity']).size()
	cr = DataFrame(cr).reset_index('returnQuantity')
	cr['totalReturns'] = cr['returnQuantity']*cr[0]
	cr = DataFrame(cr['totalReturns']).reset_index().groupby(column_name).sum()

	ci = data.groupby([column_name, 'quantity']).size()
	ci = DataFrame(ci).reset_index('quantity')
	ci['totalItems'] = ci['quantity']*ci[0]
	ci = DataFrame(ci['totalItems']).reset_index().groupby(column_name).sum()

	return_ratio = cr['totalReturns']/ci['totalItems'] #categorical evt?
	return_ratio = return_ratio.replace(np.inf, 1) #happens when totalItems = 0 but totalReturns > 0
	return_ratio = return_ratio.fillna(0) #happens when totalItems = 0, totalReturns = 0, usually the pathological case price=quantity=retQuantity=0

	return_ratio = (return_ratio-return_ratio.mean())/return_ratio.std()

	df = data.join(DataFrame(return_ratio), on=column_name)

	#write data via pickle

	pickle.dump(return_ratio, open(column_name+"_return_ratio.p", "wb" ))
	#load pickle
	#return_ratio = pickle.load(open( "return_ratio.p", "rb" ))

	#return feature array
	arr = np.ndarray(shape=(data.shape[0], 1))
	arr[:,0]= df[0]

	return arr

def prepare_return_ratio_test(data, column_name):

	return_ratio = pickle.load(open(column_name+"_return_ratio.p", "rb"))
	#rr_mean = return_ratio.mean()
	rr_mean = 0

	df = data.join(DataFrame(return_ratio), on=column_name)
	return_ratio = df[0].fillna(rr_mean)

	arr = np.ndarray(shape=(data.shape[0], 1))
	arr[:,0]= return_ratio

	return arr

#Left out features
def feature_order_id(data):
    data['orderID'] = pd.to_numeric(data['orderID'].str.replace('a', ''))
    return np.asarray(data['orderID']), ['orderID']

def feature_order_date(data):
    data['orderDate'] = pd.to_datetime(data['orderDate'], format='%Y-%m-%d')
    return np.asarray(data['orderDate']), ['orderDate']
    
def feature_article_id(data):
    data['articleID'] = pd.to_numeric(data['articleID'].str.replace('i', 0))
    return np.asarray(data['articleID']), ['articleID']

def feature_color_code(data):
    d = pd.to_numeric(((data['colorCode']) - np.mean(data['colorCode'])) / np.std(data['colorCode']))
    return np.asarray(d), ['colorCode']

def feature_size_code(data):
    return np.asarray(data['sizeCode']), ['sizeCode']

def feature_customer_id(data):
    data['customerID'] = pd.to_numeric(data['customerID'].str.replace('c', 0))
    return np.asarray(data['customerID']), ['customerID']
