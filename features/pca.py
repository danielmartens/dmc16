import matplotlib.pyplot as plt
import numpy as np

from sklearn.decomposition import PCA

def pca(train_data, test_data, components=20, verbose=False):
	pca = PCA(n_components=components)
	pca.fit(train_data)
	pcadat = pca.fit_transform(train_data)
	pc = pca.explained_variance_

	if verbose:
		print(pc)
		plt.bar(np.arange(pc.shape[0]), pca.explained_variance_ratio_)
		plt.show()

	new_train_data = pca.transform(train_data)
	new_test_data = pca.transform(test_data)
	if verbose:
		print("Train shape after PCA:", new_train_data.shape)
	return new_train_data, new_test_data
