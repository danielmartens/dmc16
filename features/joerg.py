
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


#data['articleID'] = data['articleID'].str.replace('a', '')
#data.groupby(['price','returnQuantity']).size()




def productGroup_feature(data):

    train_data = pd.read_csv('orders_train.txt', sep=';')
    test_data = pd.read_csv('orders_class.txt', sep=';')

    all_data = pd.concat([ train_data, test_data])
    uniPG = all_data['productGroup'].unique()

    productGroupNP = np.zeros([data.shape[0],uniPG.shape[0]])
    names = []
    for u in np.arange(uniPG.shape[0]):
        productGroupNP[:,u] = data['productGroup'].isin([uniPG[u]])
        names.append("productGroup_" + str(uniPG[u]) )

    return productGroupNP, names








