import numpy as np




def dmc16_eval(pred, true):
    return np.sum(np.absolute(true-pred))